variable "docker_host" {
  default = "unix:///var/run/docker.sock"
}

variable "container_count" {
  default = 3
}
