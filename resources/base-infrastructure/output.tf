output "subnets" {
  value = "${
    map(
      "0", "${aws_subnet.subnet-1a.id}",
      "1", "${aws_subnet.subnet-1b.id}",
      "2", "${aws_subnet.subnet-1b.id}"
    )
  }"
}

output "subnets_total" {
  value = "${var.subnets_total}"
}
